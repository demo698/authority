﻿using BitPost.Libraries.DbTools.Postgre;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PermissionService.Models;

[Table("Roles")]
[Index(nameof(Name), nameof(Mandant), IsUnique = true)]
public class Role : Entity
{
    [Required]
    public string Name {  get; set; }

    public string? Description { get; set; }
}
