﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PermissionService.Models;

[Index(nameof(UserId), nameof(RoleId), IsUnique = true)]
public class UserRole
{
    [Key]
    [Required]
    public int Id { get; set; }

    [Required]
    public int UserId { get; set; }

    [Required]
    [ForeignKey("Roles")]
    public int RoleId { get; set; }
}
