﻿using BitPost.Libraries.MessageBus;
using PermissionService.Data;
using PermissionService.Models;

namespace PermissionService.EventProcessor;


public class EventProcessor : IEventProcessor
{
    private readonly ILogger _logger;
    private readonly IServiceScopeFactory _scopeFactory;

    public IEnumerable<MessageEvent> Events { get; set; }

    public EventProcessor(IServiceScopeFactory scopeFactory, ILogger<EventProcessor> logger)
    {
        _scopeFactory = scopeFactory;
        _logger = logger;
        Initialize();
    }

    private void Initialize()
    {
        Events = new List<MessageEvent>
        {
            new MessageEvent { Sender = Microservices.MandantService, EventName = "CREATE", Task = OnMandantCreate }
        };
    }

    private async Task OnMandantCreate(string message)
    {
        _logger.LogInformation("On mandant creating event");
        using var scope = _scopeFactory.CreateScope();
        var repository = scope.ServiceProvider.GetService<IRoleRepo>();

        var user = new Role { Mandant = message, Name = "administrators", Description = "Built in administrator's role" };
        await repository.CreateRoleAsync(user);
    }
}
