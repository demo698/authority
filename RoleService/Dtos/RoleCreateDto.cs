﻿namespace PermissionService.Dtos;

public class RoleCreateDto
{
    public string Name { get; set; }

    public string? Description { get; set; }
}
