﻿namespace PermissionService.Dtos;

public class RoleUpdateDto
{
    public string? Name { get; set; }

    public string? Description { get; set; }
}
