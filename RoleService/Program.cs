using BitPost.Libraries.MessageBus;
using BitPost.Libraries.WebTools;
using PermissionService.Data;
using PermissionService.EventProcessor;
using Microsoft.EntityFrameworkCore;
using PermissionService.Services;

var builder = WebApplication.CreateBuilder(args);
builder.Configuration.AddConfigFiles("Secrets/secrets.json", "Routes/routes.json");

builder.Services.AddGrpc();
builder.Services.AddControllers();
builder.Services.AddDbContext<AppDbContext>(opt =>
    opt.UseNpgsql(builder.Configuration.GetConnectionString("Postgres_1"))); 
builder.Services.ConfigureJWT(builder.Configuration["JwtSecretKey"]);
builder.Services.AddScoped<IRoleRepo, RoleRepo>();
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddMessageBusReciever(Microservices.RoleService);

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new() { Title = "GroupService", Version = "v1" });
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "RoleService v1"));
}

app.UseRouting();

app.UseAuthentication();

app.UseAuthorization();

app.UseMiddleware<CheckTokenMiddleware>();

app.UseMiddleware<PermissionMiddleware>();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
    endpoints.MapGrpcService<RoleGrpcService>();
});

app.Run();
