﻿using AutoMapper;
using Grpc.Core;
using PermissionService.Data;
using RoleService.Grpc;
using static RoleService.Grpc.RoleService;

namespace PermissionService.Services;

public class RoleGrpcService : RoleServiceBase
{
    private readonly ILogger<RoleGrpcService> _logger;
    private readonly IRoleRepo _repository;
    private readonly IMapper _mapper;

    public RoleGrpcService(ILogger<RoleGrpcService> logger, IRoleRepo repository, IMapper mapper)
    {
        _logger = logger;
        _repository = repository;
        _mapper = mapper;
    }

    public override async Task<UserRolesResponse> GetUserRoles(UserRolesRequest request, ServerCallContext context)
    {
        var response = new UserRolesResponse();
        var roles = await _repository.GetUserRolesAsync(request.Mandant, request.UserId);
        var roleModels = roles.Select(r => _mapper.Map<RoleModel>(r));
        response.Roles.AddRange(roleModels);
        return response;
    }
}
