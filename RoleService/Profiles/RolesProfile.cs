﻿using AutoMapper;
using PermissionService.Dtos;
using PermissionService.Models;
using RoleService.Grpc;

namespace PermissionService.Profiles;

public class RolesProfile : Profile
{
    public RolesProfile()
    {
        CreateMap<Role, RoleReadDto>();
        
        CreateMap<RoleCreateDto, Role>();

        CreateMap<Role, RoleModel>();
    }
}
