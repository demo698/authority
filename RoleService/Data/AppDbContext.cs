﻿using PermissionService.Models;
using Microsoft.EntityFrameworkCore;

namespace PermissionService.Data;

public class AppDbContext : DbContext
{
    private readonly IConfiguration configuration;

    public AppDbContext(DbContextOptions<AppDbContext> opt, IConfiguration configuration) : base(opt)
    {
        this.configuration = configuration;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        var schemaName = configuration["DbSchema"];
        modelBuilder.HasDefaultSchema(schemaName);
        base.OnModelCreating(modelBuilder);
    }

    public DbSet<Role> Role { get; set; }
    public DbSet<UserRole> UserRoles { get; set; }
}
