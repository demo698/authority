﻿using PermissionService.Dtos;
using PermissionService.Models;

namespace PermissionService.Data;

public interface IRoleRepo
{
    Task<int> GetCountAsync(string mandant);

    Task<IEnumerable<Role>> GetRolesAsync(string mandant);

    Task<IEnumerable<Role>> GetRolesAsync(string mandant, IEnumerable<int> ids);

    Task<IEnumerable<Role>> GetUserRolesAsync(string mandant, int userId);

    Task<IEnumerable<int>> GetRoleIdsByUsersAsync(IEnumerable<int> userIds);

    Task<IEnumerable<int>> GetUserIdsByRolesAsync(IEnumerable<int> groupIds);

    Task<Role?> GetRoleByNameAsync(string mandant, string name);

    Task<Role?> GetRoleByIdAsync(string mandant, int id);

    Task CreateRoleAsync(Role group);

    Task UpdateRoleAsync(Role target, object source);

    Task AddUsersRolesAsync(IEnumerable<int> userIds, IEnumerable<int> roleIds);

    Task DelUsersRolesAsync(IEnumerable<int> userIds, IEnumerable<int> roleIds);
}
