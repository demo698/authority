﻿using BitPost.Libraries.Extensions.Common;
using Microsoft.EntityFrameworkCore;
using PermissionService.Models;

namespace PermissionService.Data;

public class RoleRepo : IRoleRepo
{
    private readonly AppDbContext _context;

    public RoleRepo(AppDbContext context)
    {
        _context = context;
    }

    public async Task<int> GetCountAsync(string mandant)
    {
        var count = await _context.Role.CountAsync(item => item.Mandant == mandant);
        return count;
    }

    public async Task<IEnumerable<Role>> GetRolesAsync(string mandant)
    {
        var roles = await _context.Role.Where(r => r.Mandant == mandant).ToListAsync();
        return roles;
    }

    public async Task<IEnumerable<Role>> GetRolesAsync(string mandant, IEnumerable<int> ids)
    {
        var roles = await _context.Role.Where(r => r.Mandant == mandant && ids.Contains(r.Id)).ToListAsync();
        return roles;
    }

    public async Task<IEnumerable<Role>> GetUserRolesAsync(string mandant, int userId)
    {
        return await _context.Role
            .Join(
                _context.UserRoles.Where(ur => ur.UserId == userId),
                role => role.Id,
                userRole => userRole.RoleId,
                (role, _) => role)
            .Where(role => role.Mandant == mandant.ToUpper())
            .ToListAsync();
    }

    public async Task<IEnumerable<int>> GetRoleIdsByUsersAsync(IEnumerable<int> userIds)
    {
        var userRoles = await _context.UserRoles
            .Where(ur => userIds.Contains(ur.UserId))
            .ToListAsync();
        var roleIds = userRoles.Where(ur => ur.UserId == userIds.First()).Select(ur => ur.RoleId);
        foreach (var userId in userIds)
        {
            var userRoleIds = userRoles.Where(ur => ur.UserId == userId).Select(ur => ur.RoleId);
            roleIds = roleIds.Intersect(userRoleIds);
        }
        return roleIds;
    }

    public async Task<IEnumerable<int>> GetUserIdsByRolesAsync(IEnumerable<int> roleIds)
    {
        var userRoles = await _context.UserRoles
            .Where(ur => roleIds.Contains(ur.RoleId))
            .ToListAsync();
        var userIds = userRoles.Where(ur => ur.RoleId == roleIds.First()).Select(ur => ur.UserId);
        foreach (var roleId in roleIds)
        {
            var roleUserIds = userRoles.Where(ur => ur.RoleId == roleId).Select(ur => ur.UserId);
            userIds = userIds.Intersect(roleUserIds);
        }
        return userIds;
    }

    public async Task<Role?> GetRoleByNameAsync(string mandant, string name)
    {
        var role = await _context.Role.FirstOrDefaultAsync(r => 
            r.Mandant == mandant && 
            r.Name.ToUpper() == name.ToUpper());
        return role;
    }

    public async Task<Role?> GetRoleByIdAsync(string mandant, int id)
    {
        var role = await _context.Role.FirstOrDefaultAsync(r => r.Mandant == mandant && r.Id == id);
        return role;
    }

    public async Task CreateRoleAsync(Role role)
    {
        ArgumentNullException.ThrowIfNull(role, nameof(role));
        await _context.Role.AddAsync(role);
        await _context.SaveChangesAsync();
    }

    public async Task UpdateRoleAsync(Role target, object source)
    {
        ArgumentNullException.ThrowIfNull(target, nameof(target));
        target.Assign(source);
        await _context.SaveChangesAsync();
    }

    public async Task AddUsersRolesAsync(IEnumerable<int> userIds, IEnumerable<int> roleIds)
    {
        var existUR = await _context.UserRoles
            .Where(ur => userIds.Contains(ur.UserId) && roleIds.Contains(ur.RoleId))
            .ToListAsync();
        var newUR = new List<UserRole>();
        foreach (var userId in userIds)
        {
            foreach (var roleId in roleIds)
            {
                if (!existUR.Any(ur => ur.UserId == userId && ur.RoleId == roleId) &&
                    !newUR.Any(ur => ur.UserId == userId && ur.RoleId == roleId))
                {
                    newUR.Add(new UserRole { UserId = userId, RoleId = roleId });
                }
            }
        }
        await _context.UserRoles.AddRangeAsync(newUR);
        await _context.SaveChangesAsync();
    }

    public async Task DelUsersRolesAsync(IEnumerable<int> userIds, IEnumerable<int> roleIds)
    {
        var usersRoles = await _context.UserRoles
            .Where(ur => userIds.Contains(ur.UserId) && roleIds.Contains(ur.RoleId))
            .ToListAsync();
        _context.UserRoles.RemoveRange(usersRoles);
        await _context.SaveChangesAsync();
    }
}
