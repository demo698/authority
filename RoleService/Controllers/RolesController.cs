﻿using AutoMapper;
using PermissionService.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BitPost.Libraries.MessageBus;
using PermissionService.Dtos;
using BitPost.Libraries.WebTools;
using PermissionService.Models;
using System.Reflection;
using System.Diagnostics;

namespace PermissionService.Controllers;

[Authorize]
[ApiController]
[Route("api/[controller]")]
public class RolesController : ControllerBase
{
    private readonly IRoleRepo _repository;
    private readonly IMapper _mapper;

    public RolesController(IRoleRepo repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    [HttpGet]
    [AllowAnonymous]
    [Route("is-alive")]
    public async Task<IActionResult> IsAlive()
    {
        await Task.Run(() => { });
        return Ok();
    }

    [HttpGet]
    [Route("count")]
    [Permission("R2E")]
    public async Task<ActionResult<int>> GetCount()
    {
        var mandant = HttpContext.GetMandantFromJWT();
        var count = await _repository.GetCountAsync(mandant);
        return Ok(count);
    }

    [HttpGet]
    [Permission("R2E")]
    public async Task<ActionResult<IEnumerable<RoleReadDto>>> GetRoles()
    {
        var mandant = HttpContext.GetMandantFromJWT();
        var roles = await _repository.GetRolesAsync(mandant);
        return Ok(_mapper.Map<IEnumerable<RoleReadDto>>(roles));
    }

    [HttpGet("get-by-id/{id}", Name = "GetRoleById")]
    public async Task<ActionResult<RoleReadDto>> GetRoleById(int id)
    {
        var mandant = HttpContext.GetMandantFromJWT();
        var role = await _repository.GetRoleByIdAsync(mandant, id);
        if (role is null)
        {
            return NotFound();
        }
        return Ok(_mapper.Map<RoleReadDto>(role));
    }

    [HttpGet("get-roles-by-users")]
    [Permission("R2E.RU")]
    public async Task<ActionResult<IEnumerable<int>>> GetRoleIdsByUsers([FromQuery(Name = "ids")] int[] userIds)
    {
        var ids = await _repository.GetRoleIdsByUsersAsync(userIds);
        return Ok(ids);
    }

    [HttpGet("get-users-by-roles")]
    [Permission("R2E.RU")]
    public async Task<ActionResult<IEnumerable<int>>> GetUserIdsByRoles([FromQuery(Name = "ids")] int[] roleIds)
    {
        var ids = await _repository.GetUserIdsByRolesAsync(roleIds);
        return Ok(ids);
    }

    [HttpPost("add-role-to-users")]
    [Permission("R2E.ARU")]
    public async Task<ActionResult> AddRoleToUsers([FromQuery(Name = "masterIds")] int[] userIds, [FromQuery(Name = "slaveId")] int roleId)
    {
        return await AddUsersRoles(userIds, new List<int>{ roleId });
    }

    [HttpPost("add-user-to-roles")]
    [Permission("R2E.ARU")]
    public async Task<ActionResult> AddUserToRoles([FromQuery(Name = "masterIds")] int[] roleIds, [FromQuery(Name = "slaveId")] int userId)
    {
        return await AddUsersRoles(new List<int> { userId }, roleIds);
    }

    private async Task<ActionResult> AddUsersRoles(IEnumerable<int> userIds, IEnumerable<int> roleIds)
    {
        var mandant = HttpContext.GetMandantFromJWT();
        var roles = await _repository.GetRolesAsync(mandant, roleIds);
        if (roles.Count() != roleIds.Count())
        {
            return Conflict(new { Code = "R2E:RNF", Message = "Some of roles are not found in this mandant." });
        }
        await _repository.AddUsersRolesAsync(userIds, roleIds);
        return Ok();
    }

    [HttpDelete("del-roles-from-users")]
    [Permission("R2E.DRU")]
    public async Task<ActionResult> DelRolesFromUsers([FromQuery(Name = "masterIds")] int[] userIds, [FromQuery(Name = "slaveIds")] int[] roleIds)
    {
        return await DelUsersRolesAsync(userIds, roleIds);
    }

    [HttpDelete("del-users-from-roles")]
    [Permission("R2E.DRU")]
    public async Task<ActionResult> DelUsersFromRoles([FromQuery(Name = "masterIds")] int[] roleIds, [FromQuery(Name = "slaveIds")] int[] userIds)
    {
        return await DelUsersRolesAsync(userIds, roleIds);
    }

    private async Task<ActionResult> DelUsersRolesAsync(IEnumerable<int> userIds, IEnumerable<int> roleIds)
    {
        var mandant = HttpContext.GetMandantFromJWT();
        var roles = await _repository.GetRolesAsync(mandant, roleIds);
        if (roles.Count() != roleIds.Count())
        {
            return Conflict(new { Code = "R2E:RNF", Message = "Some of roles are not found in this mandant." });
        }
        await _repository.DelUsersRolesAsync(userIds, roleIds);
        return Ok();
    }

    [HttpPost]
    [Permission("R2E.ADD")]
    public async Task<ActionResult<RoleReadDto>> CreateRole(RoleCreateDto roleCreateDto)
    {
        var mandant = HttpContext.GetMandantFromJWT();
        var roleExist = await _repository.GetRoleByNameAsync(mandant, roleCreateDto.Name);
        if (roleExist is not null)
        {
            return Conflict(new { Code = "R2E:RAE", Message = $"Role {roleCreateDto.Name} allready exists." });
        }

        var role = _mapper.Map<Role>(roleCreateDto);
        role.Mandant = mandant;
        await _repository.CreateRoleAsync(role);
        return Ok(_mapper.Map<RoleReadDto>(role));
    }

    [HttpPatch("{id}")]
    [Permission("R2E.EDIT")]

    public async Task<ActionResult<RoleReadDto>> UpdateRole(int id, RoleUpdateDto patch)
    {
        var mandant = HttpContext.GetMandantFromJWT();
        var role = await _repository.GetRoleByIdAsync(mandant, id);
        if (role is null)
        {
            return NotFound();
        }
        await _repository.UpdateRoleAsync(role, patch);
        return Ok(_mapper.Map<RoleReadDto>(role));
    }
}

