﻿using PermissionService.Dtos;
using PermissionService.Models;

namespace PermissionService.Data;

public interface IPermissionRepo
{
    Task<int> GetCountAsync();

    Task<IEnumerable<Permission>> GetPermissionsAsync();

    Task<IEnumerable<Permission>> GetLoginPermissionsAsync(int userId, IEnumerable<int> roleIds);

    Task<IEnumerable<int>> GetPermissionIdsByUsersAsync(IEnumerable<int> userIds);

    Task<IEnumerable<int>> GetPermissionIdsByRolesAsync(IEnumerable<int> roleIds);

    Task<IEnumerable<int>> GetUserIdsByPermissionsAsync(IEnumerable<int> permissionIds);

    Task<IEnumerable<int>> GetRoleIdsByPermissionsAsync(IEnumerable<int> permissionIds);

    Task<Permission?> GetPermissionByIdAsync(int id);

    Task CreatePermissionAsync(Permission group);

    Task UpdatePermissionAsync(Permission target, object source);

    Task AddUsersPermissionsAsync(IEnumerable<int> userIds, IEnumerable<int> permissionIds);

    Task DelUsersPermissionsAsync(IEnumerable<int> userIds, IEnumerable<int> permissionIds);

    Task AddRolesPermissionsAsync(IEnumerable<int> roleIds, IEnumerable<int> permissionIds);

    Task DelRolesPermissionsAsync(IEnumerable<int> roleIds, IEnumerable<int> permissionIds);
}
