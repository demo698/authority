﻿using BitPost.Libraries.Extensions.Common;
using Microsoft.EntityFrameworkCore;
using PermissionService.Models;

namespace PermissionService.Data;

public class PermissionRepo : IPermissionRepo
{
    private readonly AppDbContext _context;

    public PermissionRepo(AppDbContext context)
    {
        _context = context;
    }

    public async Task<int> GetCountAsync()
    {
        var count = await _context.Permissions.CountAsync();
        return count;
    }

    public async Task<IEnumerable<Permission>> GetPermissionsAsync()
    {
        return await _context.Permissions.ToListAsync();
    }

    public async Task<IEnumerable<Permission>> GetLoginPermissionsAsync(int userId, IEnumerable<int> roleIds)
    {
        var userPermissionIds = await _context.UserPermissions
            .Where(up => up.UserId == userId)
            .Select(up => up.PermissionId)
            .ToListAsync();
        var rolePermissionIds = await _context.RolePermissions
            .Where(rp => roleIds.Contains(rp.RoleId))
            .Select(rp => rp.PermissionId)
            .ToListAsync(); ;
        var permissions = await _context.Permissions
            .Where(p => rolePermissionIds.Contains(p.Id) || userPermissionIds.Contains(p.Id))
            .ToListAsync(); ;
        return permissions;
    }

    public async Task<IEnumerable<int>> GetPermissionIdsByUsersAsync(IEnumerable<int> userIds)
    {
        var userPermissions = await _context.UserPermissions
            .Where(up => userIds.Contains(up.UserId))
            .ToListAsync();
        var permissionIds = userPermissions
            .Where(up => up.UserId == userIds.First())
            .Select(up => up.PermissionId);
        foreach (var userId in userIds)
        {
            var userPermissionIds = userPermissions.Where(up => up.UserId == userId).Select(up => up.PermissionId);
            permissionIds = permissionIds.Intersect(userPermissionIds);
        }
        return permissionIds;
    }

    public async Task<IEnumerable<int>> GetPermissionIdsByRolesAsync(IEnumerable<int> roleIds)
    {
        var rolePermissions = await _context.RolePermissions
            .Where(rp => roleIds.Contains(rp.RoleId))
            .ToListAsync();
        var permissionIds = rolePermissions
            .Where(rp => rp.RoleId == roleIds.First())
            .Select(rp => rp.PermissionId);
        foreach (var roleId in roleIds)
        {
            var rolePermissionIds = rolePermissions.Where(rp => rp.RoleId == roleId).Select(rp => rp.PermissionId);
            permissionIds = permissionIds.Intersect(rolePermissionIds);
        }
        return permissionIds;
    }

    public async Task<IEnumerable<int>> GetUserIdsByPermissionsAsync(IEnumerable<int> permissionIds)
    {
        var userPermissions = await _context.UserPermissions
            .Where(up => permissionIds.Contains(up.PermissionId))
            .ToListAsync();
        var userIds = userPermissions
            .Where(up => up.PermissionId == permissionIds.First())
            .Select(up => up.UserId);
        foreach (var permissionId in permissionIds)
        {
            var permissionUserIds = userPermissions.Where(up => up.PermissionId == permissionId).Select(up => up.UserId);
            userIds = userIds.Intersect(permissionUserIds);
        }
        return userIds;
    }

    public async Task<IEnumerable<int>> GetRoleIdsByPermissionsAsync(IEnumerable<int> permissionIds)
    {
        var rolePermissions = await _context.RolePermissions
            .Where(rp => permissionIds.Contains(rp.PermissionId))
            .ToListAsync();
        var roleIds = rolePermissions
            .Where(rp => rp.PermissionId == permissionIds.First())
            .Select(rp => rp.RoleId);
        foreach (var permissionId in permissionIds)
        {
            var permissionRoleIds = rolePermissions.Where(rp => rp.PermissionId == permissionId).Select(rp => rp.RoleId);
            roleIds = roleIds.Intersect(permissionRoleIds);
        }
        return roleIds;
    }

    public async Task<Permission?> GetPermissionByIdAsync(int id)
    {
        return await _context.Permissions.FirstOrDefaultAsync(p => p.Id == id);
    }

    public async Task CreatePermissionAsync(Permission permission)
    {
        ArgumentNullException.ThrowIfNull(permission, nameof(permission));
        _context.Permissions.Add(permission);
        await _context.SaveChangesAsync();
    }

    public async Task UpdatePermissionAsync(Permission target, object source)
    {
        ArgumentNullException.ThrowIfNull(target, nameof(target));
        target.Assign(source);
        await _context.SaveChangesAsync();
    }

    public async Task AddUsersPermissionsAsync(IEnumerable<int> userIds, IEnumerable<int> permissionIds)
    {
        var existingUP = await _context.UserPermissions
            .Where(up => userIds.Contains(up.UserId) && permissionIds.Contains(up.PermissionId))
            .ToListAsync();
        var newUP = new List<UserPermission>();

        foreach (var userId in userIds)
        {
            foreach (var permissionId in permissionIds)
            {
                if (!existingUP.Any(up => up.UserId == userId && up.PermissionId == permissionId) &&
                    !newUP.Any(up => up.UserId == userId && up.PermissionId == permissionId))
                {
                    newUP.Add(new UserPermission { UserId = userId, PermissionId = permissionId });
                }
            }
        }
        await _context.UserPermissions.AddRangeAsync(newUP);
        await _context.SaveChangesAsync();
    }

    public async Task AddRolesPermissionsAsync(IEnumerable<int> roleIds, IEnumerable<int> permissionIds)
    {
        var existingRP = await _context.RolePermissions
            .Where(rp => roleIds.Contains(rp.RoleId) && permissionIds.Contains(rp.PermissionId))
            .ToListAsync();
        var newRP = new List<RolePermission>();

        foreach (var roleId in roleIds)
        {
            foreach (var permissionId in permissionIds)
            {
                if (!existingRP.Any(rp => rp.RoleId == roleId && rp.PermissionId == permissionId) &&
                    !newRP.Any(rp => rp.RoleId == roleId && rp.PermissionId == permissionId))
                {
                    newRP.Add(new RolePermission { RoleId = roleId, PermissionId = permissionId });
                }
            }
        }
        await _context.RolePermissions.AddRangeAsync(newRP);
        await _context.SaveChangesAsync();
    }

    public async Task DelUsersPermissionsAsync(IEnumerable<int> userIds, IEnumerable<int> permissionIds)
    {
        var userPermissions = await _context.UserPermissions
            .Where(up => userIds.Contains(up.UserId) && permissionIds.Contains(up.PermissionId))
            .ToListAsync();
        _context.UserPermissions.RemoveRange(userPermissions);
        await _context.SaveChangesAsync();
    }

    public async Task DelRolesPermissionsAsync(IEnumerable<int> roleIds, IEnumerable<int> permissionIds)
    {
        var rolePermissions = await _context.RolePermissions.Where(rp =>
            roleIds.Contains(rp.RoleId) &&
            permissionIds.Contains(rp.PermissionId))
            .ToListAsync();
        _context.RolePermissions.RemoveRange(rolePermissions);
        await _context.SaveChangesAsync();
    }
}
