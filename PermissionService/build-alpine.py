import os

folder_name = os.path.split(os.getcwd())[1]
service_name = folder_name.replace('Service','').lower()
image_name = 'evm72011/bitpost-{service}-service'.format(service = service_name)
depl_name  = '{service}s-depl'.format(service = service_name)

with open('Dockerfile', 'w') as f:
    f.writelines([
        'FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine AS build\n',
        'WORKDIR /app\n',
        'ENV GLIBC_REPO=https://github.com/sgerrand/alpine-pkg-glibc\n',
        'ENV GLIBC_VERSION=2.34-r0\n',
        'RUN set -ex && \\\n',
        '    apk --update add libstdc++ curl ca-certificates && \\\n',
        '    for pkg in glibc-${GLIBC_VERSION} glibc-bin-${GLIBC_VERSION}; \\\n',
        '        do curl -sSL ${GLIBC_REPO}/releases/download/${GLIBC_VERSION}/${pkg}.apk -o /tmp/${pkg}.apk; done && \\\n',
        '    apk add --allow-untrusted /tmp/*.apk && \\\n',
        '    rm -v /tmp/*.apk && \\\n',
        '    /usr/glibc-compat/sbin/ldconfig /lib /usr/glibc-compat/lib\n',
        'COPY . ./\n',
        'RUN dotnet publish {folder} -c Release -o out\n'.format(folder = folder_name),
        '\n',
        'FROM mcr.microsoft.com/dotnet/aspnet:6.0-alpine\n',
        'WORKDIR /app\n',
        'COPY --from=build /app/out .\n',
        'ENTRYPOINT ["dotnet", "{name}.dll"]'.format(name = folder_name)
    ])
f.close()

os.system('docker build -f Dockerfile -t {image} ..'.format(image = image_name))
#os.system('docker push {image}'.format(image = image_name))
os.system('kubectl rollout restart deployment {depl} --namespace=bitpost'.format(depl = depl_name))