import os
from datetime import datetime

folder_name = os.path.split(os.getcwd())[1]
now = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
migration_name = folder_name + '_' + now
os.system("dotnet ef migrations add " + migration_name)
os.system("dotnet ef database update")