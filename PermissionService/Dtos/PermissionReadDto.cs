﻿namespace PermissionService.Dtos;

public class PermissionReadDto
{
    public int Id {  get; set; }

    public string Hash { get; set; }

    public string Name {  get; set; }

    public string Description {  get; set; }
}
