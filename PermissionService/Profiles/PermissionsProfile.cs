﻿using AutoMapper;
using PermissionService.Dtos;
using PermissionService.Models;

namespace PermissionService.Profiles;

public class PermissionsProfile : Profile
{
    public PermissionsProfile()
    {
        CreateMap<Permission, PermissionReadDto>();
    }
}
