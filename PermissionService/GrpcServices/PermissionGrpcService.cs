﻿using Grpc.Core;
using PermissionService.Data;
using PermissionService.Grpc;
using static PermissionService.Grpc.PermissionService;

namespace PermissionService.GrpcServices;

public class PermissionGrpcService: PermissionServiceBase
{
    private readonly IPermissionRepo _repository;

    public PermissionGrpcService(IPermissionRepo repository)
    {
        _repository = repository;
    }

    public override async Task<PermissionsResponse> GetPermissions(PermissionsRequest request, ServerCallContext context)
    {
        var response = new PermissionsResponse();
        var mandant = request.Mandant;
        var userId = request.UserId;
        var roleIds = request.RoleIds;
        var permissions = (await _repository.GetLoginPermissionsAsync(userId, roleIds)).ToList();
        permissions.ForEach(p => response.Permissions.Add(p.Name));
        return response;
    }

}

