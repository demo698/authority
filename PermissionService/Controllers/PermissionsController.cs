﻿using AutoMapper;
using PermissionService.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PermissionService.Dtos;
using BitPost.Libraries.WebTools;
using PermissionService.Models;

namespace PermissionService.Controllers;

[Authorize]
[ApiController]
[Route("api/[controller]")]
public class PermissionsController : ControllerBase
{
    private readonly IPermissionRepo _repository;
    private readonly IMapper _mapper;

    public PermissionsController(IPermissionRepo repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    [HttpGet]
    [AllowAnonymous]
    [Route("is-alive")]
    public async Task<IActionResult> IsAlive()
    {
        await Task.Run(() => { });
        return Ok();
    }

    [HttpGet]
    [Route("count")]
    [Permission("P9S")]

    public async Task<ActionResult<int>> GetCount()
    {
        var count = await _repository.GetCountAsync();
        return Ok(count);
    }

    [HttpGet]
    [Permission("P9S")]
    public async Task<ActionResult<IEnumerable<PermissionReadDto>>> GetPermissions()
    {
        var permissions = await _repository.GetPermissionsAsync();
        return Ok(_mapper.Map<IEnumerable<PermissionReadDto>>(permissions));
    }

    [HttpGet("get-by-id/{id}")]
    public async Task<ActionResult<PermissionReadDto>> GetPermissionById(int id)
    {
        var permission = await _repository.GetPermissionByIdAsync(id);
        if (permission is null)
        {
            return NotFound();
        }
        return Ok(_mapper.Map<PermissionReadDto>(permission));
    }

    [HttpGet("get-permissions-by-users")]
    [Permission("P9S.PU")]
    public async Task<ActionResult<IEnumerable<int>>> GetPermissionIdsByUsers([FromQuery(Name = "ids")] int[] userIds)
    {
        var ids = await _repository.GetPermissionIdsByUsersAsync(userIds);
        return Ok(ids);
    }

    [HttpGet("get-users-by-permissions")]
    [Permission("P9S.PU")]
    public async Task<ActionResult<IEnumerable<int>>> GetUserIdsByPermissions([FromQuery(Name = "ids")] int[] permissionIds)
    {
        var ids = await _repository.GetUserIdsByPermissionsAsync(permissionIds);
        return Ok(ids);
    }

    [HttpPost("add-permission-to-users")]
    [Permission("P9S.APU")]
    public async Task<ActionResult> AddPermissionToUsers([FromQuery(Name = "masterIds")] int[] userIds, [FromQuery(Name = "slaveId")] int permissionId)
    {
        return await AddUsersPermissions(userIds, new List<int>{ permissionId });
    }

    [HttpPost("add-user-to-permissions")]
    [Permission("P9S.APU")]
    public async Task<ActionResult> AddUserToPermissions([FromQuery(Name = "masterIds")] int[] roleIds, [FromQuery(Name = "slaveId")] int userId)
    {
        return await AddUsersPermissions(new List<int> { userId }, roleIds);
    }

    private async Task<ActionResult> AddUsersPermissions(IEnumerable<int> userIds, IEnumerable<int> permissionIds)
    {
        await _repository.AddUsersPermissionsAsync(userIds, permissionIds);
        return Ok();
    }

    [HttpDelete("del-permissions-from-users")]
    [Permission("P9S.DPU")]
    public async Task<ActionResult> DelPermissionFromUsers([FromQuery(Name = "masterIds")] int[] userIds, [FromQuery(Name = "slaveIds")] int[] permissionIds)
    {
        return await DelUsersPermissions(userIds, permissionIds);
    }

    [HttpDelete("del-users-from-permissions")]
    [Permission("P9S.DPU")]
    public async Task<ActionResult> DelUsersFromPermission([FromQuery(Name = "masterIds")] int[] permissionIds, [FromQuery(Name = "slaveIds")] int[] userIds)
    {
        return await DelUsersPermissions(userIds, permissionIds);
    }

    private async Task<ActionResult> DelUsersPermissions(int[] userIds, int[] permissionIds)
    {
        await _repository.DelUsersPermissionsAsync(userIds, permissionIds);
        return Ok();
    }

    [HttpGet("get-permissions-by-roles")]
    [Permission("P9S.PR")]
    public async Task<ActionResult<IEnumerable<int>>> GetPermissionIdsByRoles([FromQuery(Name = "ids")] int[] roleIds)
    {
        var ids = await _repository.GetPermissionIdsByRolesAsync(roleIds);
        return Ok(ids);
    }

    [HttpGet("get-roles-by-permissions")]
    [Permission("P9S.PR")]
    public async Task<ActionResult<IEnumerable<int>>> GetRoleIdsByPermissions([FromQuery(Name = "ids")] int[] permissionIds)
    {
        var ids = await _repository.GetRoleIdsByPermissionsAsync(permissionIds);
        return Ok(ids);
    }

    [HttpPost("add-permission-to-roles")]
    [Permission("P9S.APR")]
    public async Task<ActionResult> AddPermissionToRoles([FromQuery(Name = "masterIds")] int[] roleIds, [FromQuery(Name = "slaveId")] int permissionId)
    {
        return await AddRolesPermissions(roleIds, new List<int> { permissionId });
    }

    [HttpPost("add-role-to-permissions")]
    [Permission("P9S.APR")]
    public async Task<ActionResult> AddRoleToPermissions([FromQuery(Name = "masterIds")] int[] permissionIds, [FromQuery(Name = "slaveId")] int roleId)
    {
        return await AddRolesPermissions(new List<int> { roleId }, permissionIds);
    }

    private async Task<ActionResult> AddRolesPermissions(IEnumerable<int> roleIds, IEnumerable<int> permissionIds)
    {
        await _repository.AddRolesPermissionsAsync(roleIds, permissionIds);
        return Ok();
    }

    [HttpDelete("del-permissions-from-roles")]
    [Permission("P9S.DPR")]
    public async Task<ActionResult> DelPermissionFromRoles([FromQuery(Name = "masterIds")] int[] roleIds, [FromQuery(Name = "slaveIds")] int[] permissionIds)
    {
        return await DelRolesPermissions(roleIds, permissionIds);
    }

    [HttpDelete("del-roles-from-permissions")]
    [Permission("P9S.DPR")]
    public async Task<ActionResult> DelRolesFromPermission([FromQuery(Name = "masterIds")] int[] permissionIds, [FromQuery(Name = "slaveIds")] int[] roleIds)
    {
        return await DelRolesPermissions(roleIds, permissionIds);
    }

    private async Task<ActionResult> DelRolesPermissions(int[] roleIds, int[] permissionIds)
    {
        await _repository.DelRolesPermissionsAsync(roleIds, permissionIds);
        return Ok();
    }
}

