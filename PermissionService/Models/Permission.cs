﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PermissionService.Models;

[Table("Permissions")]
public class Permission
{
    [Key]
    [Required]
    public int Id { get; set; }

    [Required]
    public string Name {  get; set; }

    public string? Description { get; set; }
}
