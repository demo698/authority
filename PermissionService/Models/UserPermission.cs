﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PermissionService.Models;

[Index(nameof(UserId), nameof(PermissionId), IsUnique = true)]
public class UserPermission
{
    [Key]
    [Required]
    public int Id { get; set; }

    [Required]
    public int UserId { get; set; }

    [Required]
    [ForeignKey("Permission")]
    public int PermissionId { get; set; }
}
