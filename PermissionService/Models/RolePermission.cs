﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PermissionService.Models;

[Index(nameof(RoleId), nameof(PermissionId), IsUnique = true)]
public class RolePermission
{
    [Key]
    [Required]
    public int Id { get; set; }

    [Required]
    public int RoleId { get; set; }

    [Required]
    [ForeignKey("Permission")]
    public int PermissionId { get; set; }
}
