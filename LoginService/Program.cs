using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using BitPost.Libraries.WebTools;
using LoginService.GrpcClients;
using static LoginService.Grpc.Mandant.MandantService;
using static LoginService.Grpc.User.UserService;
using static LoginService.Grpc.Role.RoleService;
using static LoginService.Grpc.Permission.PermissionService;
using GrpcTools.RetryPolicies;
using System;

var builder = WebApplication.CreateBuilder(args);
builder.Configuration.AddConfigFiles("Secrets/secrets.json", "Routes/routes.json");

builder.Services.AddControllers();
builder.Services.ConfigureCORS();
builder.Services.ConfigureJWT(builder.Configuration["JwtSecretKey"]);

builder.Services.AddGrpcClient<MandantServiceClient>(opt =>
{
    opt.Address = new Uri(builder.Configuration["MandantServiceGrpc"]);
}).AddPolicyHandler(Policies.RetryFunc);
builder.Services.AddGrpcClient<UserServiceClient>(opt =>
{
    opt.Address = new Uri(builder.Configuration["UserServiceGrpc"]);
}).AddPolicyHandler(Policies.RetryFunc);
builder.Services.AddGrpcClient<RoleServiceClient>(opt =>
{
    opt.Address = new Uri(builder.Configuration["RoleServiceGrpc"]);
}).AddPolicyHandler(Policies.RetryFunc);
builder.Services.AddGrpcClient<PermissionServiceClient>(opt =>
{
    opt.Address = new Uri(builder.Configuration["PermissionServiceGrpc"]);
}).AddPolicyHandler(Policies.RetryFunc);

builder.Services.AddSingleton<IMandantGrpcClient, MandantGrpcClient>();
builder.Services.AddSingleton<IUserGrpcClient, UserGrpcClient>();
builder.Services.AddSingleton<IRoleGrpcClient, RoleGrpcClient>();
builder.Services.AddSingleton<IPermissionGrpcClient, PermissionGrpcClient>();

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new() { Title = "LoginService", Version = "v1" });
});

var app = builder.Build();
if (builder.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "LoginService v1"));
}
app.UseCors();
app.MapControllers();
app.UseAuthentication();
app.UseAuthorization();
app.UseMiddleware<CheckTokenMiddleware>();
app.Run();
