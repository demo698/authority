﻿
namespace LoginDataService.Models;

public class LoginData
{
    public string Mandant { get; set; }

    public string Login { get; set; }
    
    public string Password {  get; set; }

    public bool IsInvalid
    {
        get => string.IsNullOrWhiteSpace(Mandant) ||
               string.IsNullOrWhiteSpace(Login) ||
               string.IsNullOrWhiteSpace(Password);
    }

    public void Deconstruct(out string mandant, out string userName, out string password)
    {
        mandant = Mandant;
        userName = Login;
        password = Password;
    }
}
