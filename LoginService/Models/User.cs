﻿namespace LoginService.Models;

public class User
{
    public int Id { get; set; }

    public string Hash { get; set; }

    public string Name { get; set; }

    public string EMail { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public bool IsLocked { get; set; }

    public bool IsReleaser { get; set; }

    public bool NeedReleaser { get; set; }

    public bool PasswordExpired { get; set; }
}
