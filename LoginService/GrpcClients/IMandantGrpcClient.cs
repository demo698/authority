﻿using LoginService.Grpc.Mandant;
using System.Threading.Tasks;

namespace LoginService.GrpcClients;

public interface IMandantGrpcClient
{
    Task<ValidateResponse> ValidateMandantAsync(string mandant);
}
