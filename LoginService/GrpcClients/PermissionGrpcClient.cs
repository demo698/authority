﻿using LoginService.Grpc.Permission;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static LoginService.Grpc.Permission.PermissionService;

namespace LoginService.GrpcClients;

public class PermissionGrpcClient: IPermissionGrpcClient
{
    private readonly PermissionServiceClient _client;

    public PermissionGrpcClient(PermissionServiceClient client)
    {
        _client = client;
    }

    public async Task<PermissionsResponse> GetPermissionsAsync(string mandant, int userId, IEnumerable<int> roleIds)
    {
        var request = new PermissionsRequest
        {
            Mandant = mandant,
            UserId = userId,
        };
        roleIds.ToList().ForEach(roleId => request.RoleIds.Add(roleId));
        return await _client.GetPermissionsAsync(request);
    }
}
