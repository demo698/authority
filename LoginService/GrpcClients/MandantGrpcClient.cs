﻿using LoginService.Grpc.Mandant;
using System.Threading.Tasks;
using static LoginService.Grpc.Mandant.MandantService;

namespace LoginService.GrpcClients;

public class MandantGrpcClient : IMandantGrpcClient
{
    private readonly MandantServiceClient _client;

    public MandantGrpcClient(MandantServiceClient client)
    {
        _client = client;
    }

    public async Task<ValidateResponse> ValidateMandantAsync(string mandant)
    {
        return await _client.ValidateMandantAsync(new ValidateRequest
        {
            Mandant = mandant
        });
    }
}
