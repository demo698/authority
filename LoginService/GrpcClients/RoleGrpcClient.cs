﻿using LoginService.Grpc.Role;
using System.Threading.Tasks;
using static LoginService.Grpc.Role.RoleService;

namespace LoginService.GrpcClients;

public class RoleGrpcClient : IRoleGrpcClient
{
    private readonly RoleServiceClient _client;

    public RoleGrpcClient(RoleServiceClient client)
    {
        _client = client;
    }

    public async Task<UserRolesResponse> GetUserRolesAsync(string mandant, int userId)
    {
        return await _client.GetUserRolesAsync(new UserRolesRequest
        {
            Mandant = mandant,
            UserId = userId
        });
    }
}
