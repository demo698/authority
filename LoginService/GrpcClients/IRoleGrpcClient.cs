﻿using LoginService.Grpc.Role;
using System.Threading.Tasks;

namespace LoginService.GrpcClients;

public interface IRoleGrpcClient
{
    Task<UserRolesResponse> GetUserRolesAsync(string mandant, int userId);
}
