﻿using LoginService.Grpc.Permission;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LoginService.GrpcClients;

public interface IPermissionGrpcClient
{
    Task<PermissionsResponse> GetPermissionsAsync(string mandant, int userId, IEnumerable<int> roleIds);
}
