﻿using LoginService.Grpc.User;
using System.Threading.Tasks;

namespace LoginService.GrpcClients;

public interface IUserGrpcClient
{
    Task<UserValidateResponse> ValidateUserAsync(string mandant, int userId);
    Task<UserValidateResponse> ValidateUserAsync(string mandant, string userName, string password);
}
