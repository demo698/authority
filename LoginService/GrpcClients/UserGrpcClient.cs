﻿using LoginService.Grpc.User;
using System.Threading.Tasks;
using static LoginService.Grpc.User.UserService;

namespace LoginService.GrpcClients;

public class UserGrpcClient: IUserGrpcClient
{
    private readonly UserServiceClient _client;

    public UserGrpcClient(UserServiceClient client)
    {
        _client = client;
    }

    public async Task<UserValidateResponse> ValidateUserAsync(string mandant, string login, string password)
    {
        return await _client.ValidateByLoginAsync(new ValidateByLoginRequest
        {
            Mandant = mandant,
            Login = login,
            Password = password
        });
    }

    public async Task<UserValidateResponse> ValidateUserAsync(string mandant, int userId)
    {
        return await _client.ValidateByIdAsync(new ValidateByIdRequest
        {
            Mandant = mandant,
            UserId = userId
        });
    }
}
