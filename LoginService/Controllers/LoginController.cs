﻿using BitPost.Libraries.WebTools;
using LoginDataService.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using LoginService.GrpcClients;

namespace LoginDataService.Controllers;

[Authorize]
[ApiController]
[Route("api/[controller]")]
public class LoginController : ControllerBase
{
    private readonly IConfiguration _configuration;
    private readonly IRoleGrpcClient _roleClient;
    private readonly IPermissionGrpcClient _permissionClient;
    private readonly IMandantGrpcClient _mandantClient;
    private readonly IUserGrpcClient _userClient;

    public LoginController(IConfiguration configuration, IRoleGrpcClient roleClient,
        IPermissionGrpcClient permissionClient, IMandantGrpcClient mandantClient, IUserGrpcClient userClient)
    {
        _configuration = configuration;
        _roleClient = roleClient;
        _permissionClient = permissionClient;
        _mandantClient = mandantClient;
        _userClient = userClient;
    }

    [HttpPost]
    [AllowAnonymous]
    public async Task<ActionResult<dynamic>> Login(LoginData loginData)
    {
        if (loginData is null || loginData.IsInvalid)
        {
            return BadRequest(new { Code = "L3N:LNC", Message = "Login information is not complete" });
        }
        try
        {
            var taskMandant = _mandantClient.ValidateMandantAsync(loginData.Mandant);
            var (mandant, login, password) = loginData;
            var taskUser = _userClient.ValidateUserAsync(mandant, login, password);
            await Task.WhenAll(taskMandant, taskUser);
            var responseMandant = await taskMandant;
            var responseUser = await taskUser;

            if (!responseMandant.IsValid)
            {
                return Unauthorized(new { responseMandant.Code, responseMandant.Message });
            }
            if (!responseUser.IsValid)
            {
                return Unauthorized(new { responseUser.Code, responseUser.Message });
            }
            var permissions = await GetPermissionsAsync(mandant, responseUser.UserId, responseUser.UserName);
            return Ok( new { token = CreateToken(loginData.Mandant, responseUser.UserId, permissions) } );
        }
        catch (Exception ex)
        {
            return StatusCode(500, ex.Message);
        }
    }

    [HttpGet("check-token")]
    public async Task<ActionResult<string>> CheckToken()
    {
        // TODO: is not DRY
        var mandant = HttpContext.GetMandantFromJWT();
        var userId = HttpContext.GetUserIdFromJWT();
        try
        {
            var taskMandant = _mandantClient.ValidateMandantAsync(mandant);
            var taskUser = _userClient.ValidateUserAsync(mandant, userId);
            await Task.WhenAll(new Task[] { taskMandant, taskUser });
            var responseMandant = await taskMandant;
            var responseUser = await taskUser;
            if (!responseMandant.IsValid)
            {
                return Unauthorized(new { responseMandant.Code, responseMandant.Message });
            }
            if (!responseUser.IsValid)
            {
                return Unauthorized(new { responseUser.Code, responseUser.Message });
            }
            var permissions = await GetPermissionsAsync(mandant, responseUser.UserId, responseUser.UserName);
            return Ok(new { token = CreateToken(mandant, responseUser.UserId, permissions) });
        }
        catch (Exception ex)
        {
            return StatusCode(500, new { Code = "L3N:UER", Message = $"Unexpected error: {ex.Message}" });
        }
    }

    private string CreateToken(string mandant, int userId, string permissions)
    {
        var jwtKey = _configuration["JwtSecretKey"];
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(jwtKey);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new Claim[]
            {
                    new Claim("MANDANT", mandant),
                    new Claim("USER_ID", userId.ToString()),
                    new Claim("PERMISSIONS", permissions)
            }),
            Expires = DateTime.UtcNow.AddDays(7),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }

    private async Task<string> GetPermissionsAsync(string mandant, int userId, string userName)
    {
        if (userName == "admin")
        {
            return "$FULL.ACCESS";
        }

        var responseRoles = await _roleClient.GetUserRolesAsync(mandant, userId);
        if (responseRoles.Roles.Any(r => r.Name == "administrators"))
        {
            return "$FULL.ACCESS";
        }
        var roleIds = responseRoles.Roles.Select(r => r.Id);
        foreach (var item in responseRoles.Roles)
        {
            Console.WriteLine(item.Name);
        }
        var responsePermissions = await _permissionClient.GetPermissionsAsync(mandant, userId, roleIds);
        return string.Join(';', responsePermissions.Permissions);
    }
}
