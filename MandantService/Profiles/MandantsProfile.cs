﻿
using AutoMapper;
using MandantService.Dtos;
using MandantService.Models;
using System;

namespace MandantService.Profiles;

public class MandantsProfile : Profile
{
    public MandantsProfile()
    {
        CreateMap<Mandant, MandantReadDto>();

        CreateMap<MandantCreateDto, Mandant>()
            .ForMember(dest => dest.Expiration, opt => opt.MapFrom(src => DateTime.SpecifyKind(DateTime.Now.AddYears(1), DateTimeKind.Utc)));

        //CreateMap<MandantReadDto, MessagePrototype>()
        //    .ForMember(dest => dest.Sender, opt => opt.MapFrom(src => MessageSender.MandantService));

        //CreateMap<MandantReadDto, MandantMessage>()
        //    .IncludeBase<MandantReadDto, MessagePrototype>();
    }
}
