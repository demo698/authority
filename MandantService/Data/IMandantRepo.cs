﻿using MandantService.Dtos;
using MandantService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MandantService.Data;

public interface IMandantRepo
{
    Task<IEnumerable<Mandant>> GetMandantsAsync();

    Task<Mandant?> GetMandantAsync(string id);

    Task CreateMandantAsync(Mandant mandant);

    Task UpdateMandantAsync(Mandant target, object source);
}
