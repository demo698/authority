﻿
using MandantService.Models;
using BitPost.Libraries.Extensions.Common;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MandantService.Data;

public class MandantRepo : IMandantRepo
{
    private readonly AppDbContext context;

    public MandantRepo(AppDbContext context)
    {
        this.context = context;
    }

    public async Task<IEnumerable<Mandant>> GetMandantsAsync()
    {
        var mandants = await context.Mandants.ToListAsync();
        return mandants;
    }


    public async Task<Mandant?> GetMandantAsync(string id)
    {
        var mandant = await context.Mandants.FirstOrDefaultAsync(m => m.Id == id);
        return mandant;
    }

    public async Task CreateMandantAsync(Mandant mandant)
    {
        ArgumentNullException.ThrowIfNull(mandant, nameof(mandant));
        await context.Mandants.AddAsync(mandant);
        await context.SaveChangesAsync();
    }

    public async Task UpdateMandantAsync(Mandant target, object source)
    {
        ArgumentNullException.ThrowIfNull(target, nameof(target));
        target.Assign(source);
        await context.SaveChangesAsync();
    }
}
