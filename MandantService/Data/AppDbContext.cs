﻿
using MandantService.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;

namespace MandantService.Data;

public class AppDbContext : DbContext
{
    private readonly IConfiguration configuration;

    public AppDbContext(DbContextOptions<AppDbContext> opt, IConfiguration configuration) : base(opt)
    {
        this.configuration = configuration;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        var schemaName = configuration["DbSchema"];
        Console.WriteLine("--> Schema name: " + schemaName);
        modelBuilder.HasDefaultSchema(schemaName);
        base.OnModelCreating(modelBuilder);
    }

    public DbSet<Mandant> Mandants { get; set; }
}
