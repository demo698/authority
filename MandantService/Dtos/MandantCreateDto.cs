﻿
namespace MandantService.Dtos;

public class MandantCreateDto
{
    public string Id { get; set; }
    public string Description { get; set; }
}
