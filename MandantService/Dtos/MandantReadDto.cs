﻿
using System;

namespace MandantService.Dtos;

public class MandantReadDto
{
    public string Id { get; set; }

    public string Description { get; set; }

    public bool Disabled { get; set; }

    public DateTime Expiration { get; set; }

    public bool IsLocked { get; set; }
}
