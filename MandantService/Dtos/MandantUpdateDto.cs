﻿using System;

namespace MandantService.Dtos;

public class MandantUpdateDto
{
    public string? Description { get; set; }

    public bool? IsLocked { get; set; }

    public DateTime? Expiration { get; set; }
}
