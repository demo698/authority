﻿using AutoMapper;
using BitPost.Libraries.MessageBus;
using BitPost.Libraries.WebTools;
using MandantService.Data;
using MandantService.Dtos;
using MandantService.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MandantService.Controllers;

[Authorize]
[ApiController]
[Route("api/[controller]")]
public class MandantsController : ControllerBase
{
    private readonly IMandantRepo _repository;
    private readonly IMapper _mapper;
    private readonly IMessageBusSender _messageBusSender;

    public MandantsController(IMandantRepo repository, IMapper mapper, IMessageBusSender messageBusSender)
    {
        _repository = repository;
        _mapper = mapper;
        _messageBusSender = messageBusSender;
    }

    [HttpGet]
    [AllowAnonymous]
    [Route("is-alive")]
    public async Task<IActionResult> IsAlive()
    {
        await Task.Run(() => { });
        return Ok();
    }

    [HttpGet]
    [Permission("GOOD")]
    public async Task<ActionResult<IEnumerable<MandantReadDto>>> GetMandants()
    {
        var mandants = await _repository.GetMandantsAsync();
        return Ok(_mapper.Map<IEnumerable<MandantReadDto>>(mandants));
    }

    [HttpGet("{id}", Name = "GetMandant")]
    [Permission("GOOD")]
    public async Task<ActionResult<MandantReadDto>> GetMandant(string id)
    {
        var mandant = await _repository.GetMandantAsync(id);
        if (mandant is null)
        {
            return NotFound(new { Code = "M5T:MNF", Message = $"Mandant {id} not found." });
        }
        return Ok(_mapper.Map<MandantReadDto>(mandant));
    }

    [HttpPost]
    [Permission("GOOD")]
    public async Task<ActionResult<MandantReadDto>> CreateMandant(MandantCreateDto mandantCreateDto)
    {
        var mandant = await _repository.GetMandantAsync(mandantCreateDto.Id);
        if (mandant is not null)
        {
            return Conflict(new { Code = "M5T:MAE", Message = $"Mandant {mandantCreateDto.Id} allready exists." });
        }
        if (!Mandant.IsNameValid(mandantCreateDto.Id))
        {
            return BadRequest(new { Code = "M5T:MNE", Message = $"Mandant's name error (pattern: {Mandant.IdPattern})" });
        }

        mandant = _mapper.Map<Mandant>(mandantCreateDto);
        await _repository.CreateMandantAsync(mandant);
        var mandantReadDto = _mapper.Map<MandantReadDto>(mandant);

        _messageBusSender.SendMessage("CREATE", mandantReadDto.Id);

        return Ok(mandantReadDto);
    }

    [HttpPatch("{id}")]
    [Permission("GOOD")]
    public async Task<ActionResult<MandantReadDto>> UpdateMandant(string id, MandantUpdateDto patch)
    {
        var mandant = await _repository.GetMandantAsync(id);
        await _repository.UpdateMandantAsync(mandant, patch);
        return Ok(_mapper.Map<MandantReadDto>(mandant));
    }
}
