﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;

namespace MandantService.Models;

public enum MandantsState
{
    Valid,
    Expired,
    Locked
}

public class Mandant
{
    public static readonly string IdPattern = @"^[A-Z0-9_-]{1,32}$";
    private string id;

    [Key]
    [Required]
    [StringLength(32)]
    public string Id {
        get => id;
        set {
            if (!IsNameValid(value))
            {
                throw new ArgumentException($"Mandant's id must be: {IdPattern}", nameof(Id));
            }
            id = value;
        }
    }

    public string? Description { get; set; }

    public bool IsLocked { get; set; } = false;

    public DateTime Expiration { get; set; }

    public static bool IsNameValid(string name)
    {
        if (string.IsNullOrEmpty(name)) return false;
        var regexValidator = new RegexStringValidator(IdPattern);
        try
        {
            regexValidator.Validate(name);
        }
        catch
        {
            return false;
        }
        return true;
    }

    public MandantsState State
    {
        get
        {
            if (IsLocked)
            {
                return MandantsState.Locked;
            }
            if (Expiration < DateTime.Now)
            {
                return MandantsState.Expired;
            }
            return MandantsState.Valid;
        }
    }
}

