using BitPost.Libraries.MessageBus;
using BitPost.Libraries.WebTools;
using MandantService.Data;
using MandantService.GrpcServices;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

var builder = WebApplication.CreateBuilder(args);
builder.Configuration.AddConfigFiles("Secrets/secrets.json", "Routes/routes.json");

builder.Services.AddGrpc();
builder.Services.AddControllers();
builder.Services.AddDbContext<AppDbContext>(opt =>
    opt.UseNpgsql(builder.Configuration.GetConnectionString("Postgres_1")));
builder.Services.AddScoped<IMandantRepo, MandantRepo>();
builder.Services.AddMessageBusSender(Microservices.MandantService);
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new() { Title = "MandantService", Version = "v1" });
});

var app = builder.Build();

if (builder.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "MandantService v1"));
}

app.UseRouting();

app.UseAuthentication();

app.UseAuthorization();

app.UseMiddleware<CheckTokenMiddleware>();

app.UseMiddleware<PermissionMiddleware>();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
    endpoints.MapGrpcService<MandantGrpcService>();
});

app.Run();
