﻿using Grpc.Core;
using MandantService.Data;
using MandantService.Grpc.Mandant;
using MandantService.Models;
using System.Threading.Tasks;
using static MandantService.Grpc.Mandant.MandantService;

namespace MandantService.GrpcServices;

public class MandantGrpcService : MandantServiceBase
{
    private readonly IMandantRepo _repository;

    public MandantGrpcService(IMandantRepo mandantRepo)
    {
        _repository = mandantRepo;
    }

    public override async Task<ValidateResponse> ValidateMandant(ValidateRequest request, ServerCallContext context)
    {
        var mandant = await _repository.GetMandantAsync(request.Mandant);
        var (isValid, code, message) = mandant?.State switch
        {
            null                  => (false, "M5T:MNF", "Mandant not found"),
            MandantsState.Locked  => (false, "M5T:MLC", "Mandant is locked"),
            MandantsState.Expired => (false, "M5T:MEX", "Mandant is expired"),
            _ => (true, string.Empty, string.Empty)
        };
        return new ValidateResponse
        {
            IsValid = isValid,
            Code = code,
            Message = message,
        };
    }
}
