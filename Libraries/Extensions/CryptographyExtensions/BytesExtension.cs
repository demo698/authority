﻿using System.Security.Cryptography;
using System.Text;

namespace BitPost.Libraries.Extensions.Cryptography;

public static class BytesExtension
{
    public static string ToMDF5(this byte[] data)
    {
        var md5 = MD5.Create();
        var bytes = md5.ComputeHash(data);
        var sb = new StringBuilder();
        foreach (var item in bytes)
        {
            sb.Append(item.ToString("x2"));
        }
        return sb.ToString();
    }
}
