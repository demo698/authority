﻿using System.Text;
using System.Security.Cryptography;

namespace BitPost.Libraries.Extensions.Cryptography;

public static class StringExtension
{
    public static string ToMD5(this string str)
    {
        if (string.IsNullOrEmpty(str))
        {
            return "";
        }
        using MD5 md5 = MD5.Create();
        var buffer = Encoding.ASCII.GetBytes(str);
        var bytes = md5.ComputeHash(buffer);
        var sb = new StringBuilder();
        foreach (var item in bytes)
        {
            sb.Append(item.ToString("x2"));
        }
        return sb.ToString();
    }

    public static string ToSha256(this string str)
    {
        if (string.IsNullOrEmpty(str))
        {
            return "";
        }
        var sha = SHA256.Create();
        var buffer = Encoding.ASCII.GetBytes(str);
        var bytes = sha.ComputeHash(buffer);
        var sb = new StringBuilder();
        foreach (var item in bytes)
        {
            sb.Append(item.ToString("x2"));
        }
        return sb.ToString();
    }
}
