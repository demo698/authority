﻿using System.Text.Json;

namespace BitPost.Libraries.Extensions.Common;

public static class ObjectExtension
{
    public static T Assign<T>(this T target, object patch)
    {
        ArgumentNullException.ThrowIfNull(target, nameof(target));
        if (patch is null)
        {
            return target;
        }
        Type targetType = target.GetType();

        foreach (var patchProperty in patch.GetType().GetProperties())
        {
            var patchValue = patchProperty.GetValue(patch, null);
            if (patchValue is not null)
            {
                var targetProperty = targetType.GetProperty(patchProperty.Name);
                if (targetProperty is null)
                {
                    throw new Exception($"Property {patchProperty.Name} not found in target Type {targetType.Name}");
                }
                targetProperty.SetValue(target, patchValue, null);
            }
        }
        return target;
    }

    public static string ToJSONString(this object obj)
    {
        return JsonSerializer.Serialize(obj);
    }

    public static string ToJSONStringF(this Object obj)
    {
        string result = "";
        int indent = 0;
        var jsonStr = obj.ToJSONString();
        foreach (var symbol in jsonStr)
        {
            if ("{[".Contains(symbol))
            {
                result += symbol;
                indent += 2;
                result += "\n" + new String(' ', indent);
            }
            else if ("}]".Contains(symbol))
            {
                indent -= 2;
                result += "\n" + new String(' ', indent) + symbol;
            }
            else if (",".Contains(symbol))
            {
                result += symbol + "\n" + new String(' ', indent);
            }
            else
                result += symbol;
        }
        return result;
    }
}
