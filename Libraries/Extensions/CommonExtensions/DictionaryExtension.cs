﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BitPost.Libraries.Extensions.Common;

public class ParameterInfo
{
    public string Name { get; set; }
    public bool Success { get; set; } = true;
    public string Message { get; set; }

}

public class ParameterInfo<T> : ParameterInfo
{
    public T? Value { get; set; }
}

public static class DictionaryExtension
{
    public static void AdaptAfterDeserialisation(this Dictionary<string, object?> dictionary)
    {
        foreach (var key in dictionary.Keys)
        {
            var val = dictionary[key];
            if (val is null)
            {
                dictionary[key] = null;
                continue;
            }
            var value = (JsonElement)val;
            dynamic newValue = value.ValueKind switch
            {
                JsonValueKind.Null or JsonValueKind.Undefined => null,
                JsonValueKind.Number => value.GetDouble(),
                JsonValueKind.String => value.GetString(),
                JsonValueKind.True or JsonValueKind.False => value.GetBoolean(),
                _ => "unsupported type"
            };
            dictionary[key] = newValue;
        }
    }
}
