﻿using BitPost.Libraries.Extensions.Common;
using BitPost.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BitPost.Libraries.MethodParameters;

public class ParameterCollection
{
    private readonly List<ParameterInfo> parameterInfos = new();

    public bool HasErrors { get => parameterInfos.Any(p => !p.Success); }

    public string ErrorInfo
    {
        get
        {
            var infos = parameterInfos
                .Where(p => !p.Success)
                .Select(p => $"{p.Name}:{p.Message}");
            return string.Join(Environment.NewLine, infos);
        }
    }

    public ParameterCollection(MethodInfo method, Dictionary<string, object> dict)
    {

        var parameterAttrs = method.GetCustomAttributes<ParameterAttribute>();
        foreach (var paramAttr in parameterAttrs)
        {
            var name = paramAttr.Name;
            ParameterInfo parameterInfo = paramAttr.ParameterType switch
            {
                ParameterType.Integer => new ParameterInfo<int>(name, dict, paramAttr.Default),
                ParameterType.Byte => new ParameterInfo<byte>(name, dict, paramAttr.Default),
                ParameterType.Decimal => new ParameterInfo<decimal>(name, dict, paramAttr.Default),
                ParameterType.Boolean => new ParameterInfo<bool>(name, dict, paramAttr.Default),
                ParameterType.String  => new ParameterInfo<string>(name, dict, paramAttr.Default),
                //ParameterType.Area => new ParameterInfo<string>(name, dict, paramAttr.Default),
                _ => throw new NotSupportedException(),
            };
            parameterInfos.Add(parameterInfo);
        }
    }

    public T Get<T>(string name)
    {
        var parameterInfo = parameterInfos.FirstOrDefault(p => p.Name == name);
        if (parameterInfo == null)
        {
            throw new Exception($"Can't find parameter info for {name}");
        }
        return (parameterInfo as ParameterInfo<T>).Value;
    }

    public override string ToString()
    {
        var paramsStr = parameterInfos.Select(p => p.ToString());
        return string.Join(',', paramsStr);
    }

    public Rectangle GetRectangle()
    {
        return new Rectangle(
            Get<int>("LEFT"),
            Get<int>("TOP"),
            Get<int>("WIDTH"),
            Get<int>("HEIGHT")
        );
    }
}
