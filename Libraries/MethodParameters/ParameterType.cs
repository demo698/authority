﻿namespace BitPost.Libraries.MethodParameters;

public enum ParameterType
{
    Integer,
    Byte,
    Decimal,
    Boolean,
    String,
//    Area
}
