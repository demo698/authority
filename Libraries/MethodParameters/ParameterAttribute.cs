﻿namespace BitPost.Libraries.MethodParameters;

[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
public class ParameterAttribute : Attribute
{
    public string Name { get; set; }
    public ParameterType ParameterType { get; set; }
    public object? Default { get; set; }
    public ParameterAttribute(string name, ParameterType type, object? @default = null)
    {
        Name = name;
        ParameterType = type;
        Default = @default;
    }
}
