﻿using BitPost.Libraries.Extensions.Common;

namespace BitPost.Libraries.MethodParameters;

public class ParameterInfo
{
    public string Name { get; set; }
    public bool Success { get; set; } = true;
    public string Message { get; set; }
}

public class ParameterInfo<T> : ParameterInfo
{
    public T? Value { get; set; }

    public ParameterInfo(string name, Dictionary<string,object> dict, object? @default)
    {
        Name = name;
        if (!dict.ContainsKey(name))
        {
            Success = @default is not null;
            Message = $"Parameter {name} is not defined";
            Value = @default is null ? default : (T)@default;
            return;
        }
        try
        {
            Value = (T)Convert.ChangeType(dict[name], typeof(T));
        }
        catch (Exception ex)
        {
            Success = @default is not null;
            Message = $"Error parsing parameter {name}: {ex.Message}";
            Value = @default is null ? default : (T)@default;
        }
    }

    public override string ToString()
    {
        return $"{Name}:{Value?.ToString() ?? "null"}";
    }
}
