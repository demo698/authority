﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitPost.Libraries.WebTools;

[AttributeUsage(AttributeTargets.Method)]
public class PermissionAttribute : Attribute
{
    public string Permission { get; set; }

    public PermissionAttribute(string permission)
    {
        Permission = permission;
    }
}
