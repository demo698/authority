﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitPost.Libraries.WebTools;

public static class ConfigurationManagerExtension
{
    public static void AddConfigFiles(this ConfigurationManager configuration, params string[] files)
    {
        foreach (var file in files)
        {
            var fileName = Path.GetFileName(file);
            var filePath = Path.GetDirectoryName(file);
            var builder = new ConfigurationBuilder()
                .SetBasePath(Path.Combine(Directory.GetCurrentDirectory(), filePath))
                .AddJsonFile(fileName)
                .Build();
            configuration.AddConfiguration(builder);
        }
    }
}
