﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BitPost.Libraries.WebTools;

public static class ControllerBaseExtension
{
    public static ActionResult GetResultFromMessage(this ControllerBase controller, HttpResponseMessage message)
    {
        var contentStream = message.Content.ReadAsStream();
        try
        {
            var content = JsonSerializer.Deserialize<Dictionary<string, string>>(contentStream);
            return controller.StatusCode((int)message.StatusCode, content);
        }
        catch (Exception)
        {
            return controller.StatusCode((int)message.StatusCode);
        }
    }

    public static T GetEntityFromMessage<T>(this ControllerBase _, HttpResponseMessage message) where T : class
    {
        var contentStream = message.Content.ReadAsStream();
        try
        {
            var options = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
            return JsonSerializer.Deserialize<T>(contentStream, options);
        }
        catch (Exception)
        {
            return null;
        }
    }

    public static string GetContentFromMessage(this ControllerBase _, HttpResponseMessage message)
    {
        var contentStream = message.Content.ReadAsStream();
        var reader = new StreamReader(contentStream);
        return message.StatusCode + ":" + reader.ReadToEnd();
    }
}
