﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitPost.Libraries.WebTools;

public class PermissionMiddleware
{
    private readonly RequestDelegate _next;

    public PermissionMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(HttpContext context)
    {
        var endpoint = context.Features.Get<IEndpointFeature>()?.Endpoint;
        var attribute = endpoint?.Metadata.GetMetadata<PermissionAttribute>();
        var methodPermission = attribute?.Permission?.ToUpper();
        if (string.IsNullOrEmpty(methodPermission))
        {
            await _next(context);
            return;
        }

        var token = context.Request.Headers[HeaderNames.Authorization];
        if (string.IsNullOrEmpty(token))
        {
            context.Response.StatusCode = 403;
            return;
        }

        var tokenPermissionsStr = context.GetPermissionsFromJWT();
        if (string.IsNullOrEmpty(tokenPermissionsStr))
        {
            context.Response.StatusCode = 403;
            await context.Response.WriteAsync("There is no permission's claim in the JWT");
            return;
        }
        var tokenPermissions = tokenPermissionsStr.Split(';');

        if (methodPermission == "GOOD" && !tokenPermissions.Contains("$GOOD"))
        {
            context.Response.StatusCode = 403;
            await context.Response.WriteAsync("There is no permission's to GOOD methods");
            return;
        }

        if (tokenPermissions.Contains("$FULL.ACCESS"))
        {
            await _next(context);
            return;
        }

        if (!tokenPermissions.Any(p => p.StartsWith(methodPermission)))
        {
            context.Response.StatusCode = 403;
            await context.Response.WriteAsync($"Permission {methodPermission} needed");
            return;
        }
        await _next(context);
    }
}
