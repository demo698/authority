﻿using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;

namespace BitPost.Libraries.WebTools;

public static class HttpContextExtension
{
    public static string GetMandantFromJWT(this HttpContext httpContext)
    {
        return httpContext.User.FindFirst("MANDANT")?.Value;
    }

    public static int GetUserIdFromJWT(this HttpContext httpContext)
    {
        var userIdStr = httpContext.User.FindFirst("USER_ID")?.Value;
        _ = int.TryParse(userIdStr, out int userId);
        return userId;
    }

    public static string GetPermissionsFromJWT(this HttpContext httpContext)
    {
        return httpContext.User.FindFirst("PERMISSIONS")?.Value;
    }

    public static string GetTokenError(this HttpContext httpContext)
    {
        var token = httpContext.Request.Headers[HeaderNames.Authorization];
        if (string.IsNullOrEmpty(token))
        {
            return null;
        }

        var mandant = httpContext.GetMandantFromJWT();
        if (string.IsNullOrEmpty(mandant))
        {
            return "There are no mandant's claim in the JWT";
        }

        var permissions = httpContext.GetPermissionsFromJWT();
        if (string.IsNullOrEmpty(permissions))
        {
            return "There is no permission's claim in the JWT";
        }

        var userIdStr = httpContext.User.FindFirst("USER_ID")?.Value;
        if (string.IsNullOrEmpty(userIdStr))
        {
            return "There are no user's id claim in the JWT";
        }
        var parsed = int.TryParse(userIdStr, out int _);
        if (!parsed)
        {
            return "User's id claim in not a number in the JWT";
        }
        return null;
    }
}
