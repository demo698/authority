﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitPost.Libraries.WebTools;

public class CheckTokenMiddleware
{
    private readonly RequestDelegate _next;

    public CheckTokenMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        var err = context.GetTokenError();
        if (!string.IsNullOrEmpty(err))
        {
            context.Response.StatusCode = 403;
            await context.Response.WriteAsync(err);
            return;
        }
        await _next(context);
    }
}
