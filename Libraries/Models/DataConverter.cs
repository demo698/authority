﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BitPost.Models;

public static class DataConverter
{
    public static object EncodeData(DataTypes dataType, byte[] data)
    {
        if (data is null || !data.Any()) return null;
        object result;
        try
        {
            result = dataType switch
            {
                DataTypes.String => Encoding.UTF8.GetString(data),
                DataTypes.Integer => BitConverter.ToInt32(data),
                DataTypes.Double => BitConverter.ToDouble(data),
                DataTypes.Boolean => BitConverter.ToBoolean(data),
//                DataTypes.Area => Encoding.UTF8.GetString(data),
                _ => "ERR: Unsupported type"
            };
        }
        catch (Exception)
        {
            return "ERR: Conversion error";
        }
        return result;
    }

    public static byte[] DecodeData(DataTypes dataType, object value)
    {
        var jsonVal = (JsonElement)value;
        dynamic newValue = jsonVal.ValueKind switch
        {
            JsonValueKind.Null or JsonValueKind.Undefined => null,
            JsonValueKind.Number => jsonVal.GetDouble(),
            JsonValueKind.String => jsonVal.GetString(),
            JsonValueKind.True or JsonValueKind.False => jsonVal.GetBoolean(),
            _ => "unsupported type"
        };

        return dataType switch
        {
            DataTypes.String => Encoding.UTF8.GetBytes(newValue.ToString()),
            DataTypes.Integer => BitConverter.GetBytes((int)newValue),
            DataTypes.Double => BitConverter.GetBytes((double)newValue),
            DataTypes.Boolean => BitConverter.GetBytes((bool)newValue),
//            DataTypes.Area => Encoding.UTF8.GetBytes(newValue.ToString()),
            _ => throw new Exception("Unsupported type")
        };
    }
}
