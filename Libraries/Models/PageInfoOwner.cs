﻿using System;
using System.Configuration;

namespace BitPost.Models;

public enum PageMask : int
{
    All = 0,
    First = 1,
    NotFirst = 2,
    Even = 3,
    Odd = 4,
    Last = 5,
    CustomMask = 6,
    Lambda = 7,
}

public class PageInfoOwner
{
    public PageMask PageMask { get; set; }

    public string? PageMaskCustom { get; set; }

    private Func<int, int, bool> Lambda;

    private readonly RegexStringValidator _regexValidator = new(@"^[0-9\,\-]*$");

    public PageInfoOwner()
    {
    }

    public PageInfoOwner(PageMask mask, string? customMask = null)
    {
        PageMask = mask;
        PageMaskCustom = customMask;
    }


    public bool PageMatch(int page)
    {
        return PageMask switch
        {
            PageMask.All => true,
            PageMask.First => page == 0,
            PageMask.NotFirst => page != 0,
            PageMask.Even => page % 2 == 0,
            PageMask.Odd => page % 2 != 0,
            PageMask.CustomMask => CustomMaskMatch(page + 1),
            _ => throw new ArgumentException("page count needed"),
        };
    }

    public bool PageMatch(int page, int count)
    {
        return PageMask switch
        {
            PageMask.Last => page == count - 1,
            PageMask.Lambda => Lambda(page, count),
            _ => PageMatch(page),
        };
    }

    public void SetLambda(Func<int, int, bool> lambda)
    {
        Lambda = lambda;
    }

    private bool CustomMaskMatch(int page) 
    {
        if (string.IsNullOrEmpty(PageMaskCustom)) return false;         
        try
        {
            _regexValidator.Validate(PageMaskCustom);
        }
        catch
        {
            return false;
        }
        var commaTeils = PageMaskCustom.Split(',');
        foreach (var commaTeil in commaTeils)
        {
            if (page.ToString() == commaTeil) return true;
            if (commaTeil.Contains('-'))
            {
                var splitDash = commaTeil.Split('-');
                if (splitDash.Length != 2) return false;
                if (!int.TryParse(splitDash[0], out var start)) return false;
                if (!int.TryParse(splitDash[1], out var end)) return false;
                if (start <= page && page <= end) return true;
            }
        }
        return false;
    }
}
