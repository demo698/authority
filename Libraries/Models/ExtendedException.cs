﻿using System;
using System.Net;

namespace BitPost.Models;

public class ExtendedException : Exception
{
    public string Code { get; set; }
    //public HttpStatusCode HttpCode { get; set; }

    public ExtendedException(string message)
        : base(message)
    {
    }

    public ExtendedException(string code, string message)
    : base(message)
    {
        Code = code;
    }

    //public ExtendedException(string code, string message, HttpStatusCode httpCode = HttpStatusCode.Conflict)
    //    : base(message)
    //{
    //    HttpCode = httpCode;
    //    Code = code;
    //}
}
