﻿using Google.Protobuf;
using Google.Protobuf.Collections;
using System.Text;
using System.Text.Json;

namespace BitPost.Libraries.GrpcTools;

public static class Converters
{
    // --->>
    public static RepeatedField<T1> ToGrpcRepeatedField<T1, T2>(this IEnumerable<T2> source, Func<T2, T1> map)
    {
        var result = new RepeatedField<T1>();
        source.Select(x => map(x)).ToList().ForEach(x => result.Add(x));
        return result;
    }

    public static RepeatedField<T> ToGrpcRepeatedField<T>(this IEnumerable<T> source, Func<T, T>? map = null)
    {
        var result = new RepeatedField<T>();
        map ??= (x => x);
        source.Select(x => map(x)).ToList().ForEach(x => result.Add(x));
        return result;
    }

    public static ByteString ToGrpcByteString(this Dictionary<string, object?> dic)
    {
        var options = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
        var txt = JsonSerializer.Serialize(dic, options);
        return ByteString.CopyFrom(txt, Encoding.UTF8);
    }

    // <<---
    public static List<T1> ToList<T1, T2>(this RepeatedField<T2> source, Func<T2, T1> map)
    {
        var result = new List<T1>();
        source.Select(x => map(x)).ToList().ForEach(x => result.Add(x));
        return result;
    }

    public static T Deserialize<T>(this ByteString byteString)
    {
        var bytes = byteString.ToArray();
        var str = Encoding.UTF8.GetString(bytes);
        return JsonSerializer.Deserialize<T>(str);
    }
}
