﻿using Grpc.Core;
using Polly;
using System.Net;

namespace GrpcTools.RetryPolicies;

public static class Policies
{
    private static readonly HttpStatusCode[] serverErrors = new HttpStatusCode[] {
        HttpStatusCode.BadGateway,
        HttpStatusCode.GatewayTimeout,
        HttpStatusCode.ServiceUnavailable,
        HttpStatusCode.InternalServerError,
        HttpStatusCode.TooManyRequests,
        HttpStatusCode.RequestTimeout
    };

    private static readonly StatusCode[] gRpcErrors = new StatusCode[] {
        StatusCode.DeadlineExceeded,
        StatusCode.Internal,
//        StatusCode.NotFound,
        StatusCode.ResourceExhausted,
        StatusCode.Unavailable,
        StatusCode.Unknown
    };

    public static readonly Func<HttpRequestMessage, IAsyncPolicy<HttpResponseMessage>> RetryFunc = (request) =>
    {
        return Policy.HandleResult<HttpResponseMessage>(response => {

            var grpcStatus = StatusManager.GetStatusCode(response);
            var httpStatusCode = response.StatusCode;

            return (grpcStatus == null && serverErrors.Contains(httpStatusCode)) || // if the server send an error before gRPC pipeline
                   (httpStatusCode == HttpStatusCode.OK && gRpcErrors.Contains(grpcStatus.Value)); // if gRPC pipeline handled the request (gRPC always answers OK)
        })
        .WaitAndRetryAsync(3, retryAttempt => TimeSpan.FromSeconds(2 + retryAttempt), (result, timeSpan, retryCount, context) =>
        {
            var grpcStatus = StatusManager.GetStatusCode(result.Result);
            Console.WriteLine($"Request failed with {grpcStatus}. Retry");
        });
    };
}

internal class StatusManager
{
    public static StatusCode? GetStatusCode(HttpResponseMessage response)
    {
        var headers = response.Headers;

        if (!headers.Contains("grpc-status") && response.StatusCode == HttpStatusCode.OK)
            return StatusCode.OK;

        if (headers.Contains("grpc-status"))
            return (StatusCode)int.Parse(headers.GetValues("grpc-status").First());

        return null;
    }
}
