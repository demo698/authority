﻿namespace BitPost.Libraries.DbTools.Postgre;

public interface IRepository<T> where T : Entity
{
    Task<int> GetCount(string mandant);

    Task<T?> GetItem(string mandant, int id);

    Task<IEnumerable<T>> GetItems(string mandant);

    Task<IEnumerable<T>> GetItems(string mandant, IEnumerable<int> ids);

    Task AddItem(T item);

    Task AddItems(IEnumerable<T> items);

    Task<T> UpdateItem(string mandant, int id, object patch);

    Task DeleteItem(string mandant, T item);

    Task<T?> DeleteItem(string mandant, int id);

    Task DeleteItems(string mandant, IEnumerable<T> items);

    Task DeleteItems(string mandant, IEnumerable<int> ids);

    public bool SaveChanges();
}
