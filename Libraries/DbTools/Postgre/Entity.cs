﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BitPost.Libraries.DbTools.Postgre;

public class Entity
{
    [Key]
    [Required]
    public int Id { get; set; }

    [Required]
    [StringLength(32)]
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public string Hash { get; set; }

    [Required]
    [StringLength(32)]
    public string Mandant { get; set; }
}