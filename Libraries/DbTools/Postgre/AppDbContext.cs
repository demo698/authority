﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace BitPost.Libraries.DbTools.Postgre;

public abstract class AppDbContext<T> : DbContext where T: Entity
{
    private readonly IConfiguration _configuration;

    public AppDbContext(DbContextOptions<AppDbContext<T>> opt, IConfiguration configuration) : base(opt)
    {
        _configuration = configuration;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        var schemaName = _configuration["DbSchema"];
        Console.WriteLine("--> Schema name: " + schemaName);
        modelBuilder.HasDefaultSchema(schemaName);
        base.OnModelCreating(modelBuilder);
    }

    public DbSet<T> Items { get; set; }
}
