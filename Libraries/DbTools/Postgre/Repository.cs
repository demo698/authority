﻿using BitPost.Libraries.Extensions.Common;
using Microsoft.EntityFrameworkCore;

namespace BitPost.Libraries.DbTools.Postgre;

public class Repository<T> : IRepository<T> where T : Entity
{
    private readonly AppDbContext<T> _context;
    private readonly DbSet<T> _items;

    public Repository(AppDbContext<T> context)
    {
        _context = context;
        _items = context.Items;
    }

    public Task<int> GetCount(string mandant)
    {
        return _items.CountAsync(i => i.Mandant == mandant);
    }

    public Task<T?> GetItem(string mandant, int id)
    {
        return _items.SingleOrDefaultAsync(i => i.Mandant == mandant && i.Id == id);
    }

    public Task<IEnumerable<T>> GetItems(string mandant)
    {
        var items = _items.Where(i => i.Mandant == mandant);
        return Task.FromResult(items as IEnumerable<T>);
    }

    public Task<IEnumerable<T>> GetItems(string mandant, IEnumerable<int> ids)
    {
        var items = _items.Where(i => i.Mandant == mandant && ids.Contains(i.Id));
        return Task.FromResult(items as IEnumerable<T>);
    }

    public Task AddItem(T item)
    {
        _items.Add(item);
        return Task.CompletedTask;
    }

    public Task AddItems(IEnumerable<T> items)
    {
        _items.AddRange(items);
        return Task.CompletedTask;
    }

    public Task DeleteItem(string mandant, T item)
    {
        ArgumentNullException.ThrowIfNull(item, nameof(item));
        _items.Remove(item);
        return Task.CompletedTask;
    }
    
    public async Task<T?> DeleteItem(string mandant, int id)
    {
        var item = await GetItem(mandant, id);
        if (item is not null) _items.Remove(item);
        return item;
    }

    public Task DeleteItems(string mandant, IEnumerable<T> items)
    {
        items.ToList().ForEach(item => ArgumentNullException.ThrowIfNull(item));
        _items.RemoveRange(items);
        return Task.CompletedTask;
    }

    public async Task DeleteItems(string mandant, IEnumerable<int> ids)
    {
        var items = await GetItems(mandant, ids);
        _items.RemoveRange(items);
    }

    public async Task<T> UpdateItem(string mandant, int id, object patch)
    {
        ArgumentNullException.ThrowIfNull(patch, nameof(patch));
        var item = await GetItem(mandant, id);
        if (item is not null) item.Assign(patch);
        return item;
    }

    public bool SaveChanges()
    {
        return _context.SaveChanges() > 0;
    }
}
