﻿
using BitPost.Libraries.DbTools.Postgre;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using BitPost.Libraries.Extensions.Common;
using BitPost.Libraries.Extensions.Cryptography;

namespace UserService.Models;

[Index(nameof(Name), nameof(Mandant), IsUnique = true)]
[Index(nameof(EMail), IsUnique = true)]
public class User : Entity
{
    public static readonly string NamePattern = @"^[[\.A-Za-z0-9_-]+$";
    private string name;
    private string email;

    [Required]
    public string Name
    {
        get => name;
        set
        {
            if (!value.PassPattern(NamePattern))
            {
                throw new ArgumentException($"Users's name must be: {NamePattern}", nameof(Name));
            }
            name = value.Trim();
        }
    }

    [Required]
    public string EMail
    {
        get => email;
        set
        {
            if (IsEmailValid(value))
            {
                email = value.Trim();
            }
            else
            {
                throw new ArgumentException($"Users's email format error", nameof(EMail));
            }
        }
    }

    public string? FirstName { get; set; }

    public string? LastName { get; set; }

    [Required]
    public string PasswordHash { get; set; } = "123".ToSha256();

    [Required]
    public bool IsLocked { get; set; } = false;

    [Required]
    public bool IsReleaser { get; set; } = false;

    [Required]
    public bool NeedReleaser { get; set; } = true;

    [Required]
    public bool PasswordExpired { get; set; } = true;

    [Required]
    public bool IsEmailVerified { get; set; } = false;

    private bool IsEmailValid(string email)
    {
        var trimmedEmail = email.Trim();
        if (trimmedEmail.EndsWith("."))
        {
            return false;
        }
        try
        {
            var addr = new System.Net.Mail.MailAddress(email);
            return addr.Address == trimmedEmail;
        }
        catch
        {
            return false;
        }
    }
}
