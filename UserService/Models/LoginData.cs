﻿namespace UserService.Models;

public class LoginData
{
    public string Mandant { get; set; }

    public string UserName { get; set; }

    public string Password { get; set; }

    public bool IsValid
    {
        get => Mandant is not null &&
               UserName is not null &&
               Password is not null;
    }
}
