﻿using BitPost.Libraries.Extensions.Common;

namespace UserService.Models;

public class ChangePassword
{
    // Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character:
    public static readonly string NamePattern = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$";
    public string OldPassword { get; set; }
    public string NewPassword { get; set; }

    public bool IsValid 
    {
        get => NewPassword.PassPattern(NamePattern);
    }
}
