﻿using AutoMapper;
using BitPost.Libraries.Extensions.Cryptography;
using BitPost.Libraries.WebTools;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using UserService.Data;
using UserService.Dtos;
using UserService.Models;

namespace UserService.Controllers;

[Authorize]
[ApiController]
[Route("api/[controller]")]
public class UsersController : ControllerBase
{
    private readonly IUserRepo _repository;
    private readonly IMapper _mapper;

    public UsersController(IUserRepo repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    [HttpGet]
    [Route("count")]
    [Permission("U2R")]
    public async Task<ActionResult<int>> GetCount()
    {
        var mandant = HttpContext.GetMandantFromJWT();
        var count = await _repository.GetCountAsync(mandant);
        return Ok(count);
    }

    [HttpGet]
    [AllowAnonymous]
    [Route("is-alive")]
    public async Task<IActionResult> IsAlive()
    {
        await Task.Run(() => {});
        return Ok();
    }

    [HttpGet]
    [Permission("U2R")]
    public async Task<ActionResult<IEnumerable<UserReadDto>>> GetUsers()
    {
        var mandant = HttpContext.GetMandantFromJWT();
        var users = await _repository.GetUsersAsync(mandant);
        return Ok(_mapper.Map<IEnumerable<UserReadDto>>(users));
    }

    [HttpGet("{id}", Name = "GetUserById")]
    public async Task<ActionResult<UserReadDto>> GetUserById(int id)
    {
        var mandant = HttpContext.GetMandantFromJWT();
        var user = await _repository.GetUserAsync(mandant, id);
        if (user is null)
        {
            return NotFound();
        }
        return Ok(_mapper.Map<UserReadDto>(user));
    }

    [HttpPost]
    [Permission("U2R.ADD")]
    public async Task<ActionResult<UserReadDto>> CreateUser(UserCreateDto userCreateDto)
    {
        var mandant = HttpContext.GetMandantFromJWT();
        var user = await _repository.GetUserAsync(mandant, userCreateDto.Name);
        if (user is not null)
        {
            return Conflict(new { Code = "U2R.UNE", Message = $"User with this name allready exists." });
        }
        
        user = await _repository.GetUserAsync(userCreateDto.EMail);
        if (user is not null)
        {
            return Conflict(new { Code = "U2R.UEE", Message = $"User with this email already exist." });
        }

        user = _mapper.Map<User>(userCreateDto);
        user.Mandant = mandant;
        await _repository.CreateUserAsync(user);
        // TODO Send verification email
        return Ok(_mapper.Map<UserReadDto>(user));
    }

    [HttpPatch("{id}")]
    [Permission("U2R.EDIT")]
    public async Task<ActionResult<UserReadDto>> UpdateUser(int id, UserUpdateDto patch)
    {
        var mandant = HttpContext.GetMandantFromJWT();

        var user = await _repository.GetUserAsync(mandant, id);
        if (user is null)
        {
            return NotFound(new { Code= "U2R.UNF", Message = $"User not found." });
        }

        if (!string.IsNullOrWhiteSpace(patch.EMail))
        {
            var userEqualEmail = await _repository.GetUserAsync(patch.EMail);
            if (userEqualEmail is not null && userEqualEmail.Id != id)
            {
                return Conflict(new { Code = "U2R.UEE", Message = $"User with this email already exist." });
            }
            user.IsEmailVerified = false;
        }

        await _repository.UpdateUserAsync(user, patch);
        // TODO Send verification email if email changed
        return Ok(_mapper.Map<UserReadDto>(user));
    }

    [HttpPost("password/{id}")]
    public async Task<ActionResult> ChangePassword(int id, ChangePassword data)
    {
        if (!data.IsValid)
        {
            return BadRequest(new { Code = "U2R.PII", Message = $"New password has invalid format." });
        }
        var mandant = HttpContext.GetMandantFromJWT();
        var user = await _repository.GetUserAsync(mandant, id);
        if (user is null)
        {
            return NotFound(new { Code = "U2R.UNF", Message = $"User not found." });
        }

        if (user.PasswordHash != data.OldPassword.ToSha256())
        {
            return Conflict(new { Code = "U2R.OPI", Message = $"Old password is invalid." });
        }

        var patch = new {
            PasswordHash = data.NewPassword.ToSha256(),
            PasswordExpired = false
        };
        await _repository.UpdateUserAsync(user, patch);
        return Ok();
    }
}
