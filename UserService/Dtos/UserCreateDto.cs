﻿namespace UserService.Dtos;

public class UserCreateDto
{
    public string Name { get; set; }

    public string EMail { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public bool IsLocked { get; set; }

    public bool IsReleaser { get; set; }

    public bool NeedReleaser { get; set; } = true;

    public bool PasswordExpired { get; set; } = true;
}
