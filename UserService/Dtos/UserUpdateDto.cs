﻿namespace UserService.Dtos;

public class UserUpdateDto
{
    public string EMail { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public bool IsLocked { get; set; }

    public bool IsReleaser { get; set; }

    public bool NeedReleaser { get; set; }

    public bool PasswordExpired { get; set; }
}
