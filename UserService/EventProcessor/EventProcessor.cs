﻿using BitPost.Libraries.MessageBus;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserService.Data;
using UserService.Models;

namespace UserService.EventProcessor;

public class EventProcessor : IEventProcessor
{
    private readonly ILogger _logger;
    private readonly IServiceScopeFactory _scopeFactory;

    public IEnumerable<MessageEvent> Events { get; set; }

    public EventProcessor(IServiceScopeFactory scopeFactory, ILogger<EventProcessor> logger)
    {
        _scopeFactory = scopeFactory;
        _logger = logger;
        Initialize();
    }

    private void Initialize()
    {
        Events = new List<MessageEvent>
        {
            new MessageEvent { Sender = Microservices.MandantService, EventName = "CREATE", Task = OnMandantCreate }
        };
    }

    private async Task OnMandantCreate(string message)
    {
        _logger.LogInformation("On mandant creating event");
        using var scope = _scopeFactory.CreateScope();
        var repository = scope.ServiceProvider.GetService<IUserRepo>();

        var user = new User { Mandant = message, Name = "admin", EMail = $"{message}@bitpost.com" };
        await repository.CreateUserAsync(user);
    }
}
