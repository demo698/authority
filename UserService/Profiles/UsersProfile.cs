﻿
using AutoMapper;
using UserService.Dtos;
using UserService.Models;

namespace UserService.Profiles;

public class UsersProfile : Profile
{
    public UsersProfile()
    {
        CreateMap<User, UserReadDto>();

        CreateMap<UserCreateDto, User>();

        //CreateMap<User, MessagePrototype>()
        //    .ForMember(dest => dest.Sender, opt => opt.MapFrom(src => MessageSender.UserService));

        //CreateMap<User, UserMessage>()
        //    .IncludeBase<User, MessagePrototype>();
    }
}
