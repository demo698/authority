using BitPost.Libraries.MessageBus;
using BitPost.Libraries.WebTools;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using UserService.Data;
using UserService.Services;

var builder = WebApplication.CreateBuilder(args);
builder.Configuration.AddConfigFiles("Secrets/secrets.json", "Routes/routes.json");

builder.Services.AddGrpc();
builder.Services.AddControllers();
builder.Services.AddDbContext<AppDbContext>(opt =>
    opt.UseNpgsql(builder.Configuration.GetConnectionString("Postgres_1")));
builder.Services.ConfigureJWT(builder.Configuration["JwtSecretKey"]);
builder.Services.AddScoped<IUserRepo, UserRepo>();
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddMessageBusReciever(Microservices.UserService);
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new() { Title = "UserService", Version = "v1" });
});

var app = builder.Build();

if (builder.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "UserService v1"));
}

app.UseRouting();

app.UseAuthentication();

app.UseAuthorization();

app.UseMiddleware<CheckTokenMiddleware>();

app.UseMiddleware<PermissionMiddleware>();

app.UseEndpoints(endpoints => 
{ 
    endpoints.MapControllers();
    endpoints.MapGrpcService<UserGrpcService>();
    //endpoints.MapGet("/protos/user.proto", async context =>
    //{
    //    await context.Response.WriteAsync(System.IO.File.ReadAllText("Protos/user.proto"));
    //});
});

app.Run();
