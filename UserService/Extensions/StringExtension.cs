﻿
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace UserService.Extensions;

public static class StringExtension
{
    public static bool PassPattern(this string str, string regex)
    {
        if (string.IsNullOrWhiteSpace(str))
        {
            return false;
        }
        var regexValidator = new RegexStringValidator(regex);
        try
        {
            regexValidator.Validate(str);
        }
        catch
        {
            return false;
        }
        return true;
    }
}
