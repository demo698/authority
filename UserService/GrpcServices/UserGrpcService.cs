﻿using Grpc.Core;
using BitPost.Libraries.Extensions.Cryptography;
using System.Threading.Tasks;
using UserService.Data;
using UserService.Grpc;
using UserService.Models;
using static UserService.Grpc.UserService;

namespace UserService.Services;

public class UserGrpcService : UserServiceBase
{
    private readonly IUserRepo _repository;

    public UserGrpcService(IUserRepo mandantRepo)
    {
        _repository = mandantRepo;
    }

    public override async Task<UserValidateResponse> ValidateByLogin(ValidateByLoginRequest request, ServerCallContext context)
    {
        var passwordHash = request.Password.ToSha256();
        var user = await _repository.GetUserAsync(request.Mandant, request.Login, passwordHash);
        return CreateResponse(user);
    }

    public override async Task<UserValidateResponse> ValidateById(ValidateByIdRequest request, ServerCallContext context)
    {
        var user = await _repository.GetUserAsync(request.Mandant, request.UserId);
        return CreateResponse(user);
    }

    private static UserValidateResponse CreateResponse(User? user)
    {
        if (user is null)
        {
            return new UserValidateResponse
            {
                Code = "U2R:UNF",
                Message = "User not found",
            };
        }

        if (user.IsLocked)
        {
            return new UserValidateResponse
            {
                Code = "U2R:ULC",
                Message = "User is locked",
            };
        }

        if (!user.IsEmailVerified)
        {
            return new UserValidateResponse
            {
                Code = "U2R:ENF",
                Message = "Email is not verified",
            };
        }
        return new UserValidateResponse
        {
            IsValid = true,
            UserId = user.Id,
            UserName = user.Name,
        };
    }
}
