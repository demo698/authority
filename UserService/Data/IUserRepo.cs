﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using UserService.Models;

namespace UserService.Data;

public interface IUserRepo
{
    Task<int> GetCountAsync(string mandant);

    Task<IEnumerable<User>> GetUsersAsync(string mandant);

    Task<User?> GetUserAsync(string mandant, string name);

    Task<User?> GetUserAsync(string mandant, int id);

    Task<User?> GetUserAsync(string mandant, string login, string passwordHash);
    
    Task<User?> GetUserAsync(string email);

    Task CreateUserAsync(User user);

    Task UpdateUserAsync(User target, object source);
}
