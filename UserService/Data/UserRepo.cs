﻿
using BitPost.Libraries.Extensions.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserService.Models;

namespace UserService.Data;

public class UserRepo : IUserRepo
{
    private readonly AppDbContext context;

    public UserRepo(AppDbContext context)
    {
        this.context = context;
    }

    public async Task<int> GetCountAsync(string mandant)
    {
        var count = await context.Users.CountAsync(u => u.Mandant == mandant);
        return count;
    }

    public async Task<IEnumerable<User>> GetUsersAsync(string mandant)
    {
        var users = await context.Users.Where( u => u.Mandant == mandant).ToListAsync();
        return users;
    }

    public async Task<User?> GetUserAsync(string mandant, string name)
    {
        var user = await context.Users.FirstOrDefaultAsync(u => u.Mandant == mandant && u.Name == name);
        return user;
    }

    public async Task<User?> GetUserAsync(string email)
    {
        var normalizedEmail = email.Trim().ToUpper();
        var user = await context.Users.FirstOrDefaultAsync(u => u.EMail == normalizedEmail);
        return user;
    }

    public async Task<User?> GetUserAsync(string mandant, int id)
    {
        var user = await context.Users.FirstOrDefaultAsync(u => u.Mandant == mandant && u.Id == id);
        return user;
    }

    public async Task<User?> GetUserAsync(string mandant, string login, string passwordHash)
    {
        var normalizedlogin = login.Trim().ToUpper();
        var user = await context.Users.FirstOrDefaultAsync(u => 
             u.Mandant == mandant && 
            (u.Name.ToUpper() == normalizedlogin || u.EMail.ToUpper() == normalizedlogin) && 
             u.PasswordHash == passwordHash);
        return user;
    }

    public async Task CreateUserAsync(User user)
    {
        ArgumentNullException.ThrowIfNull(user, nameof(user));
        await context.Users.AddAsync(user);
        await context.SaveChangesAsync();
    }

    public async Task UpdateUserAsync(User target, object source)
    {
        ArgumentNullException.ThrowIfNull(target, nameof(target));
        target.Assign(source);
        await context.SaveChangesAsync();
    }
}
